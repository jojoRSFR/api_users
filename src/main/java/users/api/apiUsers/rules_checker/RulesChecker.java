package users.api.apiUsers.rules_checker;

import users.api.apiUsers.exception.BadRequestExceptionCustomized;
import users.api.apiUsers.exception.ErrorMessages;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import users.api.apiUsers.model.User;
import users.api.apiUsers.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * RulesChecker is a Service that allows to check different applicable rules
 * @author  Jovan LJILJAK
 */
@Service
public class RulesChecker {

    /**
     * Method used to define whether user is an adult or not (18 years old minimum requirement)
     * @param userDateOfBirth
     * @return true or false
     */
    public static boolean isUserAnAdult(java.time.LocalDate userDateOfBirth) throws BadRequestExceptionCustomized {

        java.time.LocalDate eighteenYearsAgo = java.time.LocalDate.now().minusYears(18);
        if (userDateOfBirth.isEqual(eighteenYearsAgo) || userDateOfBirth.isBefore(eighteenYearsAgo)){
            return true;
        } else{
            throw new BadRequestExceptionCustomized(ErrorMessages.USER_NOT_AN_ADULT);
        }
    }

    /**
     * Method used to verify whether a user is a French resident
     * @param countryOfResidence
     * @return true or false
     * @throws BadRequestExceptionCustomized
     */
    public static boolean isUserAFrenchResident(User user, String countryOfResidence) throws BadRequestExceptionCustomized {
        String countryFrance = "France";

        if (countryOfResidence.equalsIgnoreCase(countryFrance)){
            user.setCountryResidence(countryOfResidence);
            return true;
        } else {
            throw new BadRequestExceptionCustomized(ErrorMessages.COUNTRY_OF_RESIDENCE_NOT_FRANCE);
        }
    }

    /**
     * Method used to verify whether a user is a male or a female
     * @param gender
     * @return true or false
     * @throws BadRequestExceptionCustomized
     */
    public static boolean isGenderValid(User user, String gender) throws BadRequestExceptionCustomized {

        if (gender.equalsIgnoreCase(Constants.GENDER_MALE) || gender.equalsIgnoreCase(Constants.GENDER_FEMALE)) {

            // set to lower case in order to maintain uniformity
            user.setGender(StringUtils.lowerCase(gender));
            return true;
        } else {
            throw new BadRequestExceptionCustomized(ErrorMessages.UNKNOWN_GENDER);
        }
    }

    /**
     * Method that allows to verify whether a telephone is valid. If so, the user's telephone will be formatted and set
     * @param user
     * @param telephone
     * @return
     * @throws BadRequestExceptionCustomized
     */

    public Boolean isTelephoneValid(User user, String telephone) throws BadRequestExceptionCustomized{

        String pattern = "^((\\+|00)\\d{1,3}( )\\d{7,12})|(0\\d{8,9})$";
        Pattern p = Pattern.compile(pattern);
        Matcher m;
        boolean b;

        m = p.matcher(telephone);
        b = m.matches();

        if (b == true) {
            user.setTelephoneNumber(formatNumTelephone(telephone));
        } else {
            throw new BadRequestExceptionCustomized(ErrorMessages.TELEPHONE_INCORRECT_FORMAT);
        }
        return Boolean.TRUE;
    }

    /**
     * Method used to format all the telephone numbers
     * @param telephone
     * @return
     * @throws BadRequestExceptionCustomized
     */

    public String formatNumTelephone(String telephone) throws BadRequestExceptionCustomized {

        if (StringUtils.isEmpty(telephone)) {
            // telephone is set to void
            return telephone;
        } else {
            String myString = new String();
            char indexZero = telephone.charAt(0);
            char indexUn = telephone.charAt(1);

            if (indexZero == '0' && indexUn == '0') {
                myString = telephone.replaceAll("\\s+", "");
                return myString;
            } else if (indexZero == '+') {
                myString = telephone.replaceAll("\\s+", "");
                String newString = "00" + myString.substring(1);
                return newString;
            } else if (indexZero == '0' && indexUn != '0') {
                myString = telephone.replaceFirst("0", "0033");
                String returnString = myString.replaceAll("\\s+", "");
                return returnString;
            } else {
                throw new BadRequestExceptionCustomized(String.format(ErrorMessages.TELEPHONE_INCORRECT_FORMAT));
            }

        }

    }

    /**
     * Method allows to verifiy whether a date is in requested format
     * @param date
     * @return localDate
     * @throws BadRequestExceptionCustomized
     */
    public java.time.LocalDate isDateValid(String date) throws BadRequestExceptionCustomized {

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        simpleDateFormat.getCalendar().setLenient(false);
        java.time.LocalDate localDate = null;
        if (!StringUtils.isEmpty(date)) {
            try {
                 simpleDateFormat.parse(date);

            } catch (ParseException e) {
                throw new BadRequestExceptionCustomized(ErrorMessages.DATE_INCORRECT_FORMAT);
            } catch (Exception e) {
                throw new BadRequestExceptionCustomized(ErrorMessages.DATE_INCORRECT_FORMAT);
            }
        } else{
            throw new BadRequestExceptionCustomized(ErrorMessages.BIRTHDATE_NOT_SPECIFIED);
        }
        // trying to parse to Local Date that will be user after
        try {
            localDate = java.time.LocalDate.parse(date);
        } catch (Exception e) {
            throw new BadRequestExceptionCustomized(ErrorMessages.DATE_INCORRECT_FORMAT);
        }

        return localDate;
    }

}
