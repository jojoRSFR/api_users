package users.api.apiUsers.dto;

import lombok.*;
import users.api.apiUsers.exception.ErrorMessages;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;

/**
 * Class - UserDto used to create a user and to return its data
 * @author Jovan LJILJAK
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDto {

    @Nullable
    @JsonProperty("id_user")
    private Integer userId;

    @Size(max = 30)
    @NotEmpty(message = ErrorMessages.USERNAME_NOT_SPECIFIED)
    @JsonProperty("username")
    private String username;

    @NotEmpty(message = ErrorMessages.BIRTHDATE_NOT_SPECIFIED)
    @JsonProperty("birth_date")
    private String birthDate;

    @NotEmpty(message = ErrorMessages.COUNTRY_OF_RESIDENCE_NOT_SPECIFIED)
    @JsonProperty("country_of_residence")
    private String countryResidence;

    @Nullable
    @JsonProperty("telephone")
    private String telephone;

    @Nullable
    @JsonProperty("gender")
    private String gender;
}
