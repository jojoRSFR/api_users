package users.api.apiUsers.controller;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import users.api.apiUsers.dto.UserDto;
import users.api.apiUsers.exception.BadRequestExceptionCustomized;
import users.api.apiUsers.exception.ErrorMessages;
import users.api.apiUsers.exception.InternalErrorCustomized;
import users.api.apiUsers.exception.NotFoundExceptionCustomized;
import users.api.apiUsers.logger.LogExecutionTime;
import users.api.apiUsers.model.User;
import users.api.apiUsers.rules_checker.RulesChecker;
import users.api.apiUsers.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import static users.api.apiUsers.utils.Constants.BASE_URL;
import static users.api.apiUsers.utils.Constants.VERSION_V1;


/**
 * Class - user controller allows to create a user and to consult a user's information
 * @author Jovan LJILJAK
 */

@Validated
@RestController
@RequestMapping(BASE_URL + VERSION_V1)
public class UserController {

    // injection of userService
    @Autowired
    UserService userService;

    // injection of rulesChecker service
    @Autowired
    RulesChecker rulesChecker;

    /****************************************************/

    /*   Create User Controller - Post operation    */

    /***************************************************/

    @PostMapping(value = "users/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)

    @Operation(description = "registerUser", summary = "Post request that allows to register a new user and to return the its information", responses = {
            @ApiResponse(responseCode = "201", description = "User successfully created", content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = UserDto.class
                    ))}),
            @ApiResponse(responseCode = "201", description = "User successfully created."),
            @ApiResponse(responseCode = "400",  description = ErrorMessages.USERNAME_NOT_SPECIFIED +"\n"+
                    ErrorMessages.BIRTHDATE_NOT_SPECIFIED +"\n"+
                    ErrorMessages.COUNTRY_OF_RESIDENCE_NOT_SPECIFIED +"\n"+ ErrorMessages.DATE_INCORRECT_FORMAT +"\n"+
            ErrorMessages.COUNTRY_OF_RESIDENCE_NOT_FRANCE +"\n"+ ErrorMessages.USER_NOT_AN_ADULT +"\n"+ ErrorMessages.UNKNOWN_GENDER +"\n"+
            ErrorMessages.TELEPHONE_INCORRECT_FORMAT ),
            @ApiResponse(responseCode = "500", description = "Technical error")
    }, requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody
            (description = "Data transfer object (DTO) used for the creation of a user", required = true, content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = UserDto.class
                    ))))
    @LogExecutionTime
    public ResponseEntity registerUser(
            @Valid @RequestBody UserDto userDto
    ) throws InternalErrorCustomized, BadRequestExceptionCustomized {

        //Creation of a new user
        User newUser = new User();

        // check attributes following the rules and set attributes
        newUser.setUsername(userDto.getUsername());
        newUser.setBirthDate(rulesChecker.isDateValid(userDto.getBirthDate()));
        rulesChecker.isUserAnAdult(newUser.getBirthDate());
        rulesChecker.isUserAFrenchResident(newUser, userDto.getCountryResidence());

        if (!StringUtils.isEmpty(userDto.getTelephone())) {
            rulesChecker.isTelephoneValid(newUser, userDto.getTelephone());
        } else {
            newUser.setTelephoneNumber(null);
        }

        if (!StringUtils.isEmpty(userDto.getGender())) {
            rulesChecker.isGenderValid(newUser, userDto.getGender());
        } else {
            newUser.setGender(null);
        }

        User savedUser = userService.saveUser(newUser);

        // Mapping user attributes to UserDTO
        UserDto userConsultationDto = new UserDto();

        userConsultationDto.setUserId(savedUser.getUserId());
        userConsultationDto.setUsername(savedUser.getUsername());
        userConsultationDto.setBirthDate(savedUser.getBirthDate().toString());
        userConsultationDto.setCountryResidence(savedUser.getCountryResidence());
        userConsultationDto.setGender(savedUser.getGender());
        userConsultationDto.setTelephone(savedUser.getTelephoneNumber());

        return new ResponseEntity<>(userConsultationDto, HttpStatus.CREATED);

    }

    /****************************************************/

    /*   Retrieve User : Controller - Get operation */

    /***************************************************/

    @GetMapping(value = "users/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "getUserByUsername", summary = "Returns user's information", responses = {
            @ApiResponse(responseCode = "200", description = "User retrieved with success", content = {@Content(
                    mediaType = "application/json",
                    array = @ArraySchema (schema = @Schema(
                            implementation = UserDto.class
                    )))}),
            @ApiResponse(responseCode = "404", description = ErrorMessages.USER_NOT_FOUND_WITH_SPECIFIED_USERNAME),
            @ApiResponse(responseCode = "404", description = ErrorMessages.USERNAME_NOT_SPECIFIED),
            @ApiResponse(responseCode = "500", description = "Technical error")


    }, parameters = {
            @Parameter(name = "username", in = ParameterIn.PATH, description = "Username", required = true),
    }
    )
    @LogExecutionTime
    public ResponseEntity getUserByUsername(
            @PathVariable(value = "username") String username
    ) throws NotFoundExceptionCustomized, InternalErrorCustomized, BadRequestExceptionCustomized {

        if (StringUtils.isEmpty(username)){
            throw new BadRequestExceptionCustomized(ErrorMessages.USERNAME_NOT_SPECIFIED);
        }

        // in case there are multiple users with the same username
        List<User> users = null;
        users = userService.findUserByUsername(username);
        List<UserDto> usersDto = new ArrayList<>();

        users.forEach(user -> {
            // Mapping model to Dto
            UserDto userConsultationDto = new UserDto();
            userConsultationDto.setUsername(user.getUsername());
            userConsultationDto.setBirthDate(user.getBirthDate().toString());
            userConsultationDto.setGender(user.getGender());
            userConsultationDto.setCountryResidence(user.getCountryResidence());
            userConsultationDto.setTelephone(user.getTelephoneNumber());
            userConsultationDto.setUserId(user.getUserId());

            usersDto.add(userConsultationDto);
        });
        return new ResponseEntity<>(usersDto, HttpStatus.OK);

    }

}