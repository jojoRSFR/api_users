package users.api.apiUsers.exception.handler;

import users.api.apiUsers.exception.*;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * CustomRestExceptionHandler handles all the exceptions that might appear during the execution and runtime and returns the same structure of errors: proper message and status code
 * @author  Jovan LJILJAK
 */

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        List<String> errors = new ArrayList<String>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        final ApiError apiError =
                new ApiError(HttpStatus.BAD_REQUEST.value(), errors.get(0));
        return handleExceptionInternal(
                ex, apiError, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        String error = ex.getParameterName() + " parameter is missing";
        logger.info(error);

        final ApiError apiError =
                new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase() + " : " + "Missing parameter");
        return new ResponseEntity<Object>(
                apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(final BindException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());
        //
        final List<String> errors = new ArrayList<String>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
        return handleExceptionInternal(ex, apiError, headers, HttpStatus.BAD_REQUEST, request);
    }


    @Override
    protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());
        //
        final String error = ex.getValue() + " value for " + ex.getPropertyName() + " should be of type " + ex.getRequiredType();

        final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(final MissingServletRequestPartException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {

        final String error = ex.getRequestPartName() + " part is missing";
        final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Object> handleNotFoundError(final NotFoundException ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        //gerer les erreurs implemente par APYS
        // demande du Ministère de remplacer NOT_FOUND par BAD_REQUEST
        final ApiError apiError =
                new ApiError(HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler({BadRequestException.class})
    public ResponseEntity<Object> handleBadRequestError(final BadRequestException ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        //gerer les erreurs implemente par APYS
        // demande du Ministère de remplacer NOT_FOUND par BAD_REQUEST
        final ApiError apiError =
               new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({PartialContentExceptionCustomized.class})
    public ResponseEntity<Object> handlePartialContent(final PartialContentExceptionCustomized ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        //gerer les erreur implemente par bacus
        final ApiError apiError =
                new ApiError(HttpStatus.PARTIAL_CONTENT.value(), ex.getMessage());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.PARTIAL_CONTENT);
    }

    @ExceptionHandler({NotFoundExceptionCustomized.class})
    public ResponseEntity<Object> handleNotFoundPersonaliseContent(final NotFoundExceptionCustomized ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        //gerer les erreur implemente par bacus
        final ApiError apiError =
                new ApiError(HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({NoContentExceptionCustomized.class})
    public ResponseEntity<Object> handleNoContentPersonalisedContent(final NoContentExceptionCustomized ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        //gerer les erreur implemente par bacus
        final ApiError apiError =
                new ApiError(HttpStatus.NO_CONTENT.value(), ex.getMessage());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler({BadRequestExceptionCustomized.class})
    public ResponseEntity<Object> handleBadRequestPersonalisedContent(final BadRequestExceptionCustomized ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        //gerer les erreur implemente par bacus
        final ApiError apiError =
                new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({InternalErrorCustomized.class})
    public ResponseEntity<Object> handleInternalErrorPersonaliseContent(final InternalErrorCustomized ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        //gerer les erreur implemente par bacus
        final ApiError apiError =
                new ApiError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        //
        final ApiError apiError =
                new ApiError(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
