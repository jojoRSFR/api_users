package users.api.apiUsers.exception;

/**
 * Class containing Error messages.
 * @author Jovan LJILJAK
 */

public class ErrorMessages {

    /**
     * Error messages during user registration
     */
    public static final String USERNAME_NOT_SPECIFIED = "Username name cannot be empty.";
    public static final String BIRTHDATE_NOT_SPECIFIED = "The user's birthdate cannot be empty.";
    public static final String COUNTRY_OF_RESIDENCE_NOT_SPECIFIED = "The user's country of residence cannot be empty.";
    public static final String COUNTRY_OF_RESIDENCE_NOT_FRANCE = "Only French residents are allowed to register.";
    public static final String UNKNOWN_GENDER = "The gender you've entered is not recognized. Please note that this field is optionnal. " +
            "In case you decide to enter a value, the only acceptable values are male or female.";
    public static final String TELEPHONE_INCORRECT_FORMAT = "The telephone number you've enter is not in the right format." +
            "If telephone number starts with 0 it will be considered as a French telephone number." +
            "In case telephone number starts with '+' or '00' it must be firstly followed by a COUNTRY CODE, than a SPACE and eventually by " +
            "the user's TELEPHONE NUMBER.";
    public static final String DATE_INCORRECT_FORMAT = "The date you've entered is wrong. The expected format is yyyy-MM-dd.";
    public static final String USER_NOT_AN_ADULT = "Only adult users are allowed to register.";


    /**
     * Error messages during user consultation
     */
    public static final String USER_NOT_FOUND_WITH_SPECIFIED_USERNAME = "No user found with this username.";





}
