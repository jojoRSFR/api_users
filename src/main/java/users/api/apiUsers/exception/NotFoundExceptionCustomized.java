package users.api.apiUsers.exception;

/**
 * NotFoundExceptionCustomized that will be used simply by passing a message and will always contain its proper code status
 * @author  Jovan LJILJAK
 */
public class NotFoundExceptionCustomized extends Exception{
    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public NotFoundExceptionCustomized(String message) {
        this.message = message;
    }
}
