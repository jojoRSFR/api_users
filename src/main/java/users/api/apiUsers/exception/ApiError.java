package users.api.apiUsers.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Error Class - defines the content of an error: code and label
 * @author  Jovan LJILJAK
 */

@Getter
@Setter
@AllArgsConstructor
public class ApiError {

    private int codeError;
    private String label;
}
