package users.api.apiUsers.exception;

/**
 * PartialContentExceptionCustomized that will be used simply by passing a message and will always contain its proper code status
 * @author  Jovan LJILJAK
 */
public class PartialContentExceptionCustomized extends Exception {

    private String message;

    public PartialContentExceptionCustomized(String message) {
        this.message = message;
    }


    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
