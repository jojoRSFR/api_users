package users.api.apiUsers.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * NoContentExceptionCustomized that will be used simply by passing a message and will always contain its proper code status
 * @author  Jovan LJILJAK
 */

@Getter
@Setter
@AllArgsConstructor
public class NoContentExceptionCustomized extends Exception {

    private String message;
}
