package users.api.apiUsers.exception;

/**
 * BadRequestExceptionCustomized that will be used simply by passing a message and will always contain its proper code status
 * @author  Jovan LJILJAK
 */

public class BadRequestExceptionCustomized extends Exception{

        @Override
        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        private String message;

    public BadRequestExceptionCustomized(String message) {
            this.message = message;
        }
    }
