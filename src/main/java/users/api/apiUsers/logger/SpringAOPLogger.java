package users.api.apiUsers.logger;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;



/**
 * Class SpringAOPLogger : defines before and after method logging as well as the time
 * @author  Jovan LJILJAK
 */

@Aspect
@Component
@Slf4j
@ConditionalOnExpression("${aspect.enabled:true}")
public class SpringAOPLogger {


    @Around("@annotation(users.api.apiUsers.logger.LogExecutionTime)")
    public Object executionTime(ProceedingJoinPoint point) throws Throwable {

        log.info("Input parameters: " + point.getArgs()[0]);

        long startTime = System.currentTimeMillis();
        Object object = point.proceed();
        long endtime = System.currentTimeMillis();
        log.info("Output: " + object.toString());
        log.info("Class Name: "+ point.getSignature().getDeclaringTypeName() +". Method Name: "+ point.getSignature().getName() + ". Time taken for Execution is : " + (endtime-startTime) +"ms");

        return object;
    }

}
