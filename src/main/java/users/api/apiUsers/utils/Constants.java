package users.api.apiUsers.utils;

/**
 * Class containing Constants.
 * @author Jovan LJILJAK
 */

public class Constants {

    // Base URL
    public static final String BASE_URL = "apiUsers/";

    // Versions
    public static final String VERSION_V1 = "v1/";

    // Genders
    public static final String GENDER_MALE = "male";
    public static  final String GENDER_FEMALE = "female";

}
