package users.api.apiUsers.repository;

import users.api.apiUsers.exception.InternalErrorCustomized;
import users.api.apiUsers.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Repository of {@link users.api.apiUsers.model.User}.
 * @author Jovan LJILJAK
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

    /**
     * Method used to save new user and to return its data from the database
     * @param user
     * @return new user's data
     * @throws InternalErrorCustomized
     */
    User save(User user);

    /**
     * Method used to retrieve user's information by specifying username
     * @return List of Users
     */
        List<User> findAllByUsernameIgnoreCase(String username);


}
