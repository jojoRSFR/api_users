package users.api.apiUsers.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.time.LocalDate;

/**
 * Class - Entity representing a User
 * @author Jovan LJILJAK
 */

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "USER")
public class User {

    /**
     * Technical ID of the user.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq")
    @SequenceGenerator(name = "user_id_seq", sequenceName = "user_id_seq", allocationSize = 1)
    @Column(name = "id_user", nullable = false)
    private Integer userId;

    /**
     * Username.
     */
    @Column(name = "username")
    private String username;

    /**
     * User's date of birth.
     */
    @Column(name = "birth_date")
    private LocalDate birthDate;

    /**
     * User's country of residence.
     */
    @Column(name = "country_of_residence")
    private String countryResidence;

    /**
     * User's telephone number.
     */
    @Column(name = "telephone")
    private String telephoneNumber;

    /**
     * User's gender.
     */
    @Column(name = "gender")
    private String gender;

}
