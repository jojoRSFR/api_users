package users.api.apiUsers.service;

import users.api.apiUsers.exception.ErrorMessages;
import users.api.apiUsers.exception.InternalErrorCustomized;
import users.api.apiUsers.exception.NotFoundExceptionCustomized;
import users.api.apiUsers.model.User;
import users.api.apiUsers.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * UserService used to establish direct link with the repository
 * @author Jovan LJILJAK
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    /**
     * Save user to the database through repository
     * @param user
     * @return user
     * @throws InternalErrorCustomized
     */
    public User saveUser(User user) throws InternalErrorCustomized {
        try {
            User newUser = userRepository.save(user);
            return newUser;
        } catch (Exception e){
            throw new InternalErrorCustomized("An error appeared during saving of the new user. The operation could not" +
                    "be executed for the following reason: "+e + ". Please retry later.");
        }
    }

    /**
     * Method used to retrieve user through repository
     * @param username
     * @return
     * @throws NotFoundExceptionCustomized
     * @throws InternalErrorCustomized
     */
    public List<User> findUserByUsername(String username) throws NotFoundExceptionCustomized, InternalErrorCustomized {

        List<User> user = new ArrayList<>();

        try {
            user = userRepository.findAllByUsernameIgnoreCase(username);
        } catch (Exception e){
            throw new InternalErrorCustomized("The user could not be retrieved from the database for the following reason: "+e);
        }

        if (user.isEmpty()){
            throw new NotFoundExceptionCustomized(ErrorMessages.USER_NOT_FOUND_WITH_SPECIFIED_USERNAME);
        }
        return user;
    }
}
