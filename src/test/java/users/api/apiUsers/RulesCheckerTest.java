package users.api.apiUsers;
import org.junit.jupiter.api.Test;
import users.api.apiUsers.exception.BadRequestExceptionCustomized;
import users.api.apiUsers.exception.ErrorMessages;
import users.api.apiUsers.model.User;
import users.api.apiUsers.rules_checker.RulesChecker;
import java.time.LocalDate;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Unit testing of the RulesChecker class
 * @author Jovan LJILJAK
 */
public class RulesCheckerTest {


    RulesChecker checker = new RulesChecker();

    /**
     * testing gender validity
     */
    @Test
    public void testGenderTrue() throws BadRequestExceptionCustomized {
        User user = new User();

       assertTrue(checker.isGenderValid(user, "male"));

    }

    /**
     * testing gender validity
     */

    @Test
    public void testGenderFalse() {
        User user = new User();

        assertThatExceptionOfType(BadRequestExceptionCustomized.class).isThrownBy(() -> {checker.isGenderValid(user, "mal");})
                .withMessage(ErrorMessages.UNKNOWN_GENDER)
                .withNoCause();
    }

    /**
     * testing if user a French resident
     */
    @Test
    public void testIfuserIsFrenchResidentTrue() throws BadRequestExceptionCustomized {
        User user = new User();

        assertTrue(checker.isUserAFrenchResident(user, "France"));

    }

    /**
     * testing user not French resident
     */
    @Test
    public void testUserNonFrench() {
        User user = new User();

        assertThatExceptionOfType(BadRequestExceptionCustomized.class).isThrownBy(() -> {checker.isUserAFrenchResident(user, "Namibie");})
                .withMessage(ErrorMessages.COUNTRY_OF_RESIDENCE_NOT_FRANCE)
                .withNoCause();

    }

    /**
     * testing user is an adult
     */
    @Test
    public void testUserIsAnAdultTrue() throws BadRequestExceptionCustomized {

        LocalDate birthDate = LocalDate.parse("1980-01-01");
        assertTrue(checker.isUserAnAdult(birthDate));

    }

    /**
     * testing user is not an adult
     */
    @Test
    public void testUserIsAnAdultFalse() throws BadRequestExceptionCustomized {

        LocalDate birthDate = LocalDate.parse("2021-01-01");
        assertThatExceptionOfType(BadRequestExceptionCustomized.class).isThrownBy(() -> {checker.isUserAnAdult(birthDate);})
                .withMessage(ErrorMessages.USER_NOT_AN_ADULT)
                .withNoCause();
    }

    /**
     * testing birth date is valid
     */
    @Test
    public void testUserBirthDateIsValid() throws BadRequestExceptionCustomized {


        LocalDate birthDateLocalDate = checker.isDateValid("1992-01-01");

        assertEquals(birthDateLocalDate.toString(), "1992-01-01");

    }

    /**
     * testing birth date is not valid
     */
    @Test
    public void testUserBirthDateIsNotValid() throws BadRequestExceptionCustomized {


        assertThatExceptionOfType(BadRequestExceptionCustomized.class).isThrownBy(() -> {checker.isDateValid("20-10-10");})
                .withMessage(ErrorMessages.DATE_INCORRECT_FORMAT)
                .withNoCause();
    }

    /**
     * testing user's telephone valid
     */
    @Test
    public void testUserIsTelephoneValid() throws BadRequestExceptionCustomized {

        User user = new User();

        assertTrue(checker.isTelephoneValid(user,"0630303030"));

    }

    /**
     * testing user's telephone non valid
     */
    @Test
    public void testUserIsTelephoneNotValidMissingSpace() {

        User user = new User();

        assertThatExceptionOfType(BadRequestExceptionCustomized.class).isThrownBy(() -> {checker.isTelephoneValid(user,"+33630303030");})
                .withMessage(ErrorMessages.TELEPHONE_INCORRECT_FORMAT)
                .withNoCause();
    }


}
