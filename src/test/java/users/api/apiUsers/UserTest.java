package users.api.apiUsers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import users.api.apiUsers.model.User;
import users.api.apiUsers.utils.Constants;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

/**
 * Unit testing of the class User
 * @author Jovan LJILJAK
 */

public class UserTest {


    User user = new User();

    @BeforeEach
    public void setUp() {

        LocalDate birthDate = LocalDate.parse("1980-01-01");
        user.setBirthDate(birthDate);
        user.setGender(Constants.GENDER_MALE);
        user.setUsername("usernameTest");
        user.setCountryResidence("France");
        user.setUserId(1);
        user.setTelephoneNumber("0610101010");
    }

    /**
     * testing username validity
     */
    @Test
    public void testUsername(){

        Assertions.assertEquals("usernameTest", user.getUsername());
    }

    /**
     * testing birthDate validity
     */
    @Test
    public void testBirthDate(){
        LocalDate birthDate = LocalDate.parse("1980-01-01");
        Assertions.assertEquals(birthDate, user.getBirthDate());
    }

    /**
     * testing country of residence validity
     */
    @Test
    public void testCountryResidence(){
        Assertions.assertEquals("France", user.getCountryResidence());
    }

    /**
     * testing id
     */
    @Test
    public void testId(){
        Assertions.assertEquals(1, user.getUserId());
    }
    /**
     * testing gender
     */
    @Test
    public void testGender(){
        Assertions.assertEquals("male", user.getGender());
    }

    /**
     * testing telephone
     */
    @Test
    public void testTelephone(){
        Assertions.assertEquals("0610101010", user.getTelephoneNumber());
    }

}
