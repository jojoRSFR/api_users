package users.api.apiUsers;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import users.api.apiUsers.dto.UserDto;
import users.api.apiUsers.exception.ErrorMessages;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static users.api.apiUsers.utils.Constants.BASE_URL;
import static users.api.apiUsers.utils.Constants.VERSION_V1;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ActiveProfiles("test")
public class UserControllerTest extends UsersApiApplicationTests{

    String nonExistingUsername = "AnthonyLeroy";

    String existingUsername = "JeanBeauvois";


    /**************************************************/

    /**      Consult User Controller Tests            */

    /**************************************************/

    @Test
    public void testGetUser__UsernameParameterMissing() throws Exception {

        mockMvc.perform(get("/" + BASE_URL + VERSION_V1 + "users/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(contentTypeJSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetUser__NonExistingUsername() throws Exception {

        mockMvc.perform(get("/" + BASE_URL + VERSION_V1 + "users/"+nonExistingUsername)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(contentTypeJSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.label", is(ErrorMessages.USER_NOT_FOUND_WITH_SPECIFIED_USERNAME)));
    }



    @Test
    public void testGetUser__OK() throws Exception {

        mockMvc.perform(get("/" + BASE_URL + VERSION_V1 + "users/"+existingUsername)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(contentTypeJSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }


    /**************************************************/

    /**      Create User Controller Tests             */

    /**************************************************/

    @Test
    public void testCreateUser__OK() throws Exception {


         mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createCorrectUser()))
                .accept(contentTypeJSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testCreateUserNoTelephone__OK() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserNoTelephone()))
                .accept(contentTypeJSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testCreateUserNoTelephoneNoGender__OK() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserNoGenderNoTelephone()))
                .accept(contentTypeJSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testCreateUserNonFrenchResident__KO() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserNonFrenchResident()))
                .accept(contentTypeJSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.label", is(ErrorMessages.COUNTRY_OF_RESIDENCE_NOT_FRANCE)));
    }

    @Test
    public void testCreateUserNoCountry__KO() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserNoCountrySpecified()))
                .accept(contentTypeJSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.label", is("countryResidence: "+ErrorMessages.COUNTRY_OF_RESIDENCE_NOT_SPECIFIED)));
    }

    @Test
    public void testCreateUserNonAdult__KO() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserNonAdult()))
                .accept(contentTypeJSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.label", is(ErrorMessages.USER_NOT_AN_ADULT)));
    }

    @Test
    public void testCreateUserNoUsername__KO() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserNoFirstName()))
                .accept(contentTypeJSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.label", is("username: "+ErrorMessages.USERNAME_NOT_SPECIFIED)));
    }


    @Test
    public void testCreateUserNoDateOfBirth__KO() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserNoDateOfBirth()))
                .accept(contentTypeJSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.label", is("birthDate: "+ErrorMessages.BIRTHDATE_NOT_SPECIFIED)));
    }

    @Test
    public void testCreateUserIncorrectGender__KO() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserIncorrectGender()))
                .accept(contentTypeJSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.label", is(ErrorMessages.UNKNOWN_GENDER)));
    }

    @Test
    public void testCreateUserGenderCaseInsensitive__OK() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserGenderCaseInsensitive()))
                .accept(contentTypeJSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testCreateUserIncorrectTelephone__KO() throws Exception {


        mockMvc.perform(post("/" + BASE_URL + VERSION_V1 + "users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serializeObjectToString(createUserincorrectTelephone__NoSpaceAfterCountryCode()))
                .accept(contentTypeJSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.label", is(ErrorMessages.TELEPHONE_INCORRECT_FORMAT)));
    }

    /**************************************************/

    /**      User Creation DTOs            */

    /**************************************************/

    public UserDto createCorrectUser(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender("male");
        dto.setBirthDate("1970-04-04");
        dto.setTelephone("0620202020");
        dto.setCountryResidence("France");

        return dto;
    }

    public UserDto createUserNoTelephone(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender("male");
        dto.setBirthDate("1970-04-04");
        dto.setTelephone(null);
        dto.setCountryResidence("France");

        return dto;
    }

    public UserDto createUserNoGenderNoTelephone(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender(null);
        dto.setBirthDate("1970-04-04");
        dto.setTelephone(null);
        dto.setCountryResidence("France");

        return dto;
    }

    public UserDto createUserNonFrenchResident(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender("male");
        dto.setBirthDate("1970-04-04");
        dto.setTelephone(null);
        dto.setCountryResidence("Italie");

        return dto;
    }

    public UserDto createUserNoCountrySpecified(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender("male");
        dto.setBirthDate("1970-04-04");
        dto.setTelephone(null);
        dto.setCountryResidence(null);

        return dto;
    }

    public UserDto createUserNonAdult(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender("male");
        dto.setBirthDate("2020-04-04");
        dto.setTelephone(null);
        dto.setCountryResidence("France");

        return dto;
    }

    public UserDto createUserNoFirstName(){

        UserDto dto = new UserDto();
        dto.setUsername(null);
        dto.setGender("male");
        dto.setBirthDate("1970-04-04");
        dto.setTelephone(null);
        dto.setCountryResidence("France");

        return dto;
    }


    public UserDto createUserNoDateOfBirth(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender("male");
        dto.setBirthDate(null);
        dto.setTelephone(null);
        dto.setCountryResidence("France");

        return dto;
    }

    public UserDto createUserIncorrectGender(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender("RAS");
        dto.setBirthDate("1970-04-04");
        dto.setTelephone(null);
        dto.setCountryResidence("France");

        return dto;
    }

    public UserDto createUserGenderCaseInsensitive(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender("maLE");
        dto.setBirthDate("1970-04-04");
        dto.setTelephone(null);
        dto.setCountryResidence("France");

        return dto;
    }

    public UserDto createUserincorrectTelephone__NoSpaceAfterCountryCode(){

        UserDto dto = new UserDto();
        dto.setUsername("JordanPouzans");
        dto.setGender("maLE");
        dto.setBirthDate("1970-04-04");
        dto.setTelephone("+33663303030");
        dto.setCountryResidence("France");

        return dto;
    }






}



