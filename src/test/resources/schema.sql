DROP TABLE if EXISTS USER;
DROP SEQUENCE if EXISTS user_id_seq;

CREATE SEQUENCE user_id_seq START WITH 1 INCREMENT BY 1;


/***************************** CREATE TABLE USER ********************************/
CREATE TABLE USER
(
  id_user                   INTEGER DEFAULT  nextval('user_id_seq')                               PRIMARY KEY NOT NULL,
  username                  CHARACTER(30)                                                         NOT NULL,
  birth_date                DATE                                                                  NOT NULL,
  country_of_residence      CHARACTER (10)                                                        NOT NULL,
  telephone                 CHARACTER (20),
  gender                    CHARACTER (6)
);







